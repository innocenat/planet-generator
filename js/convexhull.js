
// Iterative convex hull O(n^2)
function ConvexHull(points) {
    if (points.length < 3)
        return false;

    function Normal(a, b, c) {
        var vab = new THREE.Vector3();
        var vac = new THREE.Vector3();

        vab.subVectors(a,b);
        vac.subVectors(a,c);

        var n = new THREE.Vector3();
        n.crossVectors(vab,vac);

        return n;
    }

    function faceDistance(a, b, c, p) {
        var n = Normal(a, b, c);
        return n.dot(p) - n.dot(a);
    }

    function faceVisible(a, b, c, p) {
        return faceDistance(a,b,c, p) > 0;
    }

    function removeFromArray(arr, value) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][0] == value[1] && arr[i][1] == value[0]) {
                arr[i] = arr[arr.length-1];
                arr.pop();
                return true;
            }
        }
        return false;
    }

    var Faces = [ [0, 1, 2], [0, 2, 1] ];
    for (var i = 3; i < points.length; i++) {
        var edgeList = [];
        for (var j = 0; j < Faces.length; j++) {
            var face = Faces[j];
            if (faceVisible(points[face[0]], points[face[1]], points[face[2]], points[i])) {
                if (!removeFromArray(edgeList,[face[0], face[1]] ))
                    edgeList.push([face[0], face[1]]);
                if (!removeFromArray(edgeList,[face[1], face[2]] ))
                    edgeList.push([face[1], face[2]]);
                if (!removeFromArray(edgeList,[face[2], face[0]] ))
                    edgeList.push([face[2], face[0]]);

                Faces[j] = Faces[Faces.length-1];
                Faces.pop();
                j--;
            }
        }

        //edgeList.filter(function (item, pos, self) {
        //    var idx = self.indexOf([item[1], item[0]]);
        //    return idx === -1 || idx > pos;
        //});

        edgeList.forEach(function (edge) {
            Faces.push([edge[0], edge[1], i]);
        });

        if (i == 4)
           // break;
            continue;
    }

    return Faces;
}
