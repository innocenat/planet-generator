
var Planet = {};

(function (exports) {

    function Planet(seed) {
        this.mt = new MersenneTwister(seed);
    }

    Planet.prototype.generate = function (N) {
        this.face = N;
        var points = Geo3D.RandomPointSphere(N, this.mt);
        var voro = Geo3D.Voronoi(points);
        this.delaunay = voro[0];
        this.faces = voro[1];
        this.points = points;
    };

    Planet.prototype.assignRandomColor = function () {
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            face.color = new THREE.Color(Math.random(), Math.random(), Math.random());
        }
    };

    // Compute elevation by noise function
    Planet.prototype.computeElevation = function () {
        var perlin = new Noise.Octave(7, 2, this.mt);

        // Assign to world vertices (i.e. delaunay face)
        for (var i = 0; i < this.delaunay.length; i++) {
            var face = this.delaunay[i];
            face.elevation = perlin.noise((face.center.x + 1)*2, (face.center.y + 1)*2, (face.center.z + 1)*2);
        }

        // Assign average height to each face
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            var h = 0;
            for (var j = 0; j < face.edge.length; j++) {
                h += this.delaunay[face.edge[j].idx].elevation;
            }
            face.elevation = h / face.edge.length;
        }

        // Compute gradient
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            var cur_v = face.ref_point;
            for (var j = 0; j < face.edge.length; j++) {
                var dh = face.edge[j].opposite.face.elevation - face.elevation;
                var dl = Geo3D.Vec3.from(face.edge[j].opposite.face.ref_point, cur_v).length();
                face.edge[j].gradient = dh/dl;
            }
        }
    };

    // Compute randomize wind
    Planet.prototype.computeWind = function (windPoint) {
        windPoint = windPoint || 20;

        // Computer circular wind center
        var windCenter = Geo3D.RandomPointSphere(windPoint, this.mt);
        var windSpeed = [];
        for (var i = 0; i < windPoint; i++) {
            windSpeed.push((0.7 + this.mt.random() * 0.3));// * (this.mt.random() < 0.5 ? -1 : 1));
        }

        // Compute wind at each tile
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            var wind = new Geo3D.Vec3(0, 0, 0);

            for (var j = 0; j < windPoint; j++) {
                var dVec = Geo3D.Vec3.from(windCenter[j], face.ref_point);
                var cWind = Geo3D.Vec3.cross(Geo3D.Vec3.norm(face.ref_point), Geo3D.Vec3.norm(dVec));
                cWind = Geo3D.Vec3.div(Geo3D.Vec3.mul(cWind, windSpeed[j]), Math.max(1,dVec.length()));
                //console.log(cWind);
                wind = Geo3D.Vec3.add(cWind, wind);
            }

            face.wind = wind;
            //console.log(wind);
        }

        // Compute wind distributing, also against gradient
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            var dist_factor_sum = 0;
            for (var j = 0; j < face.edge.length; j++) {
                var gradient_factor = 1;
                if (face.elevation > 0) {
                    gradient_factor = Math.pow(Math.max(1, face.edge[j].gradient), 0.5);
                }
                var dist_factor = Math.max(0, Geo3D.Vec3.dot(Geo3D.Vec3.norm(Geo3D.Vec3.from(face.ref_point, face.edge[j].opposite.face.ref_point)), face.wind)) / gradient_factor;
                face.edge[j].distibute_factor = dist_factor;
                dist_factor_sum += dist_factor;
            }

            face.dist_factor = dist_factor_sum;
        }

        return windCenter;
    };

    // Computer ambient heat by setting heat base on lat/lon
    // so polar has less heat
    Planet.prototype.computeAmbientHeat = function () {
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            var cur_v = face.ref_point;
            face.heat = (0.4+(1-Math.abs(cur_v.z))*0.6)*(Math.min(1, 1-face.elevation)/2 + 0.5);
            //face.heat = 0;//face.ambient_heat;
            //face.new_heat = 0;
        }
    };

    Planet.prototype.computeAmbientMoisture = function () {
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            face.ambient_moisture = (Math.pow(Math.max(0, -face.elevation + 0.2), 0.1)) * face.heat;
            face.moisture = 0;
            face.new_moisture = 0;
        }
    };

    Planet.prototype.computePropagatedMoisture = function () {
        var absorbRate = 0.02;

        for (var __ = 0; __ < 100; __++) {
            var changed = false;
            for (var i = 0; i < this.faces.length; i++) {
                var face = this.faces[i];

                if (face.ambient_moisture < 1e-15) {
                    continue;
                }
                changed = true;

                // Absorb moisture
                var moisture_to_absorb = Math.min(absorbRate, face.ambient_moisture);
                var moisture_left = face.ambient_moisture - moisture_to_absorb;
                face.moisture += moisture_to_absorb;

                // Propagated moisture
                for (var j = 0; j < face.edge.length; j++) {
                    var sent_moisture = (face.edge[j].distibute_factor / face.dist_factor) * moisture_left;
                    face.edge[j].opposite.face.new_moisture += sent_moisture;
                }
            }

            for (var i = 0; i < this.faces.length; i++) {
                var face = this.faces[i];
                face.ambient_moisture = face.new_moisture;//*0.99;
                face.new_moisture = 0;
            }

            if (!changed)
                break;
        }

        console.log("Moisture cycle: ", __);
    };

    Planet.prototype.displaceVertices = function () {
        var FACTOR = 0.04;

        for (var i = 0; i < this.delaunay.length; i++) {
            var face = this.delaunay[i];
            var l = face.center.length();
            var desiredLength = 1 + FACTOR * Math.max(0.2, face.elevation);
            var factor = desiredLength / l;

            face.center.x *= factor;
            face.center.y *= factor;
            face.center.z *= factor;
        }
    };

    Planet.prototype.colorByElevation = function (threshold) {
        if (threshold == undefined)
            threshold = 0.2;

        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            if (face.elevation < threshold) { // ocean
                face.color = new THREE.Color((face.elevation + 1) / 2, (face.elevation + 1) / 2, 1);
            } else {
                face.color = new THREE.Color(0, 1 - face.elevation / 2, 0);
            }
        }
    };

    Planet.prototype.colorByMoisture = function (threshold) {
        if (threshold == undefined)
            threshold = 0.2;

        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            if (face.elevation < threshold) { // ocean
                face.color = new THREE.Color(face.moisture, face.moisture, 1);
            } else {
                face.color = new THREE.Color(0, face.moisture, 0);
            }
            //console.log(face.moisture);
        }
    };

    Planet.prototype.colorByHeat = function (threshold) {
        if (threshold == undefined)
            threshold = 0.2;

        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            if (face.elevation < threshold) { // ocean
                face.color = new THREE.Color(face.heat, face.heat, 1);
            } else {
                face.color = new THREE.Color(0, face.heat, 0);
            }
            //console.log(face.heat);
        }
    };

    function variateColor(c) {
        var F = 0.05;
        return new THREE.Color(c.r+(this.mt.random-0.5)*F, c.b+(this.mt.random-0.5)*F, c.g+(this.mt.random-0.5)*F)
    }

    Planet.prototype.colorByBiome = function (threshold) {
        if (threshold == undefined)
            threshold = 0.2;

        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            if (face.elevation < threshold) { // ocean
                var xx = (face.elevation + 1) / 2;
                xx *= xx;
                xx *= 0.75;
                if (face.heat < 0.4) {
                    face.color = new THREE.Color(0.8, 0.8, 0.9); // Frozen ocean
                } else
                    face.color = new THREE.Color(xx, xx, 0.8); // Ocean
            } else {
                if (face.heat < 0.4) {
                    var elev_fac = 0.5 - Math.min(1, Math.max(0, face.elevation))/2;
                    face.color = new THREE.Color(0.5, 0.5, 0.5); // snow
                    face.color.r += elev_fac;
                    face.color.g += elev_fac;
                    face.color.b += elev_fac;
                } else if (face.moisture < 0.0001) {
                    if (face.elevation < 0.5 && face.heat > 0.6) {
                        var elev_fac = 1 - Math.min(1, face.elevation);
                        face.color = new THREE.Color(0xF2C96F); // desert
                        face.color.r *= elev_fac;
                        face.color.g *= elev_fac;
                        face.color.b *= elev_fac;
                    } else if (face.elevation < 0.50) {
                        var elev_fac = (1 - Math.min(1, face.elevation))/2 + 0.5;
                        face.color = new THREE.Color( 0x8EC329 ); // Swanna
                        face.color.r *= elev_fac;
                        face.color.g *= elev_fac;
                        face.color.b *= elev_fac;
                    } else {
                        var elev_fac = (1 - Math.min(1, (face.elevation-0.4)/0.5))/2 + 0.5;
                        face.color = new THREE.Color ( 0x9C8451 ); // rock dessert
                        face.color.r *= elev_fac;
                        face.color.g *= elev_fac;
                        face.color.b *= elev_fac;
                    }
                } else {
                    if (face.moisture > 0.6 && face.elevation < 0.45) {
                        var elev_fac = (1 - Math.min(1, face.elevation));
                        face.color = new THREE.Color(0, 0.5*elev_fac, 0.3*elev_fac); // forest
                    } else if (face.moisture > 0.4 && face.elevation < 0.6) {
                        face.color = new THREE.Color(0, 0.7 - face.elevation / 2, 0.2); // glassland
                    } else if (face.moisture > 0.2) {
                        face.color = new THREE.Color(0, Math.max(0.2, 0.8 - face.elevation / 2), Math.max(0.1, 0.4 - face.elevation/2)); // highland
                    } else if (face.elevation < 0.4) {
                        face.color = new THREE.Color(0.7-face.elevation/2, 0.7-face.elevation/2, 0.5-face.elevation/2); // rock dry land
                    } else {
                        face.color = new THREE.Color(0.1+0.6-face.elevation/2, 0.1+0.5-face.elevation/2, 0.1+0.4-face.elevation/2); // rock dry high land
                    }
                }
            }
        }
    };

    Planet.prototype.geometry = function () {
        var g = new THREE.Geometry();

        // Add vertices
        for (var i = 0; i < this.delaunay.length; i++) {
            var face = this.delaunay[i];
            g.vertices.push(new THREE.Vector3(face.center.x, face.center.y, face.center.z));
        }

        // Add faces
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            var normal = new THREE.Vector3(face.normal.x, face.normal.y, face.normal.z);
            for (var j = 2; j < face.edge.length; j++) {
                var F = new THREE.Face3(face.edge[0].idx, face.edge[j].idx, face.edge[j - 1].idx, normal, face.color);
                g.faces.push(F);
            }
        }

        return g;
    };

    Planet.prototype.inverseGeometry = function () {
        var g = new THREE.Geometry();

        // Back-assign color
        var vtxColor = [];
        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            vtxColor[face.ref_point_idx] = face.color;
        }

        // Add vertices
        for (var i = 0; i < this.points.length; i++) {
            var point = this.points[i];
            g.vertices.push(new THREE.Vector3(point.x, point.y, point.z));
        }

        // Add faces
        for (var i = 0; i < this.delaunay.length; i++) {
            var face = this.delaunay[i];
            var F = new THREE.Face3(face.edge[0].idx, face.edge[1].idx, face.edge[2].idx);
            F.vertexColors = [vtxColor[face.edge[0].idx], vtxColor[face.edge[1].idx], vtxColor[face.edge[2].idx]];
            g.faces.push(F);

        }

        g.computeFaceNormals();
        g.computeVertexNormals();
        g.colorsNeedUpdate = true;
        console.log(g);
        return g;
    };

    Planet.prototype.windGeometry = function() {
        var g = new THREE.Geometry();

        for (var i = 0; i < this.faces.length; i++) {
            var face = this.faces[i];
            var nc = g.vertices.length;

            var rp = Geo3D.Vec3.mul(face.ref_point, 1.0005);

            var windS = face.wind.length();

            var targetPoint = Geo3D.Vec3.add(rp, Geo3D.Vec3.div(face.wind, 50));
            var sideVec = Geo3D.Vec3.norm(Geo3D.Vec3.cross(face.wind, rp));
            var sidePoint = Geo3D.Vec3.add(rp, Geo3D.Vec3.div(sideVec, Math.max(100,120/windS)));

            g.vertices.push(new THREE.Vector3(targetPoint.x, targetPoint.y, targetPoint.z));
            g.vertices.push(new THREE.Vector3(rp.x, rp.y, rp.z));
            g.vertices.push(new THREE.Vector3(sidePoint.x, sidePoint.y, sidePoint.z));

            g.faces.push(new THREE.Face3(nc, nc+1, nc+2));
        }

        return g;
    };

    exports.Basic = Planet;
})(Planet);
