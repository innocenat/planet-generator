
Geo3D = {};
(function (Geo3D) {
    function Vector(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    Vector.add = function (a, b) {
        return new Vector(a.x + b.x, a.y + b.y, a.z + b.z);
    };

    Vector.from = function (a, b) {
        return new Vector(b.x - a.x, b.y - a.y, b.z - a.z);
    };

    Vector.cross = function (a, b) {
        return new Vector(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    };

    Vector.dot = function (a, b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    };

    Vector.div = function (a, b) {
        return new Vector(a.x / b, a.y / b, a.z / b);
    };

    Vector.mul = function (a, b) {
        return new Vector(a.x * b, a.y * b, a.z * b);
    };

    Vector.norm = function (v) {
        return Vector.div(v, v.length());
    };

    Vector.prototype.length = function () {
        return Math.sqrt(Vector.dot(this, this));
    };

    Vector.prototype.lengthSq = function () {
        return Vector.dot(this, this);
    };

    function HalfEdge(vertex, idx) {
        this.vertex = vertex;
        this.opposite = null;
        this.next = null;
        this.prev = null;
        this.face = null;
        this.idx = idx;
    }

    HalfEdge.prototype.setOpposite = function (he) {
        this.opposite = he;
        he.opposite = this;
    };

    function Face(HalfEdges) {
        this.edge = HalfEdges;

        for (var i = 0; i < this.edge.length; i++) {
            this.edge[i].face = this;
            this.edge[i].next = this.edge[(i + 1) % this.edge.length];
            this.edge[i].prev = this.edge[(i + this.edge.length - 1) % this.edge.length];
        }

        this.visibles = [];
        this.active = true;

        this.normal = this.calculateNormal();
    }

    Face.prototype.calculateNormal = function () {
        return Vector.cross(Vector.from(this.edge[0].vertex, this.edge[1].vertex), Vector.from(this.edge[0].vertex, this.edge[2].vertex));
    };

    Face.prototype.distance = function (p) {
        var n = this.normal;
        return Vector.dot(n, p) - Vector.dot(n, this.edge[0].vertex);
    };

    Face.prototype.circumcenter = function () {
        // maths from https://gamedev.stackexchange.com/questions/60630/how-do-i-find-the-circumcenter-of-a-triangle-in-3d
        var a = this.edge[0].vertex;
        var b = this.edge[1].vertex;
        var c = this.edge[2].vertex;
        var vAB = Vector.from(a, b);
        var vAC = Vector.from(a, c);
        var abXac = this.normal;

        var m = Vector.div(Vector.add(Vector.mul(Vector.cross(abXac, vAB), vAC.lengthSq()), Vector.mul(Vector.cross(vAC, abXac), vAB.lengthSq())), 2 * abXac.lengthSq());

        return Vector.add(a, m);
    };

    Face.fromTriangle = function (a, b, c, ia, ib, ic) {
        return new Face([new HalfEdge(a, ia), new HalfEdge(b, ib), new HalfEdge(c, ic)]);
    };

    Face.prototype.visible = function (p) {
        return this.distance(p) > 1e-15;
    };

    function Hull(points) {
        if (points.length < 4) {
            throw Error("Cannot create hull!");
        }

        var plane = Face.fromTriangle(points[0], points[1], points[2], 0, 1, 2);
        for (var i = 3; i < points.length; i++) {
            if (plane.visible(points[i])) {
                break;
            }
        }
        if (i == points.length)
            throw Error("Cannot create hull!");

        var t = points[i];
        points[i] = points[3];
        points[3] = t;

        // Initial simplex
        var t0 = Face.fromTriangle(points[0], points[2], points[1], 0, 2, 1);
        var t1 = Face.fromTriangle(points[3], points[0], points[1], 3, 0, 1);
        var t2 = Face.fromTriangle(points[3], points[1], points[2], 3, 1, 2);
        var t3 = Face.fromTriangle(points[3], points[2], points[0], 3, 2, 0);

        t0.edge[0].setOpposite(t3.edge[1]);
        t0.edge[1].setOpposite(t2.edge[1]);
        t0.edge[2].setOpposite(t1.edge[1]);
        t1.edge[0].setOpposite(t3.edge[2]);
        t2.edge[0].setOpposite(t1.edge[2]);
        t3.edge[0].setOpposite(t2.edge[2]);

        var Faces = [t0, t1, t2, t3];
        var FaceStk = [0, 1, 2, 3];

        // Assign visibility
        for (var i = 4; i < points.length; i++) {
            for (var j = 0; j < Faces.length; j++) {
                if (Faces[j].visible(points[i])) {
                    Faces[j].visibles.push(i);
                    break;
                }
            }
        }

        while (FaceStk.length > 0) {
            var face = Faces[FaceStk.pop()];
            if (face.visibles.length == 0)
                continue;

            // Choose one point as hull
            var ptIdx = face.visibles[0];
            var pt = points[ptIdx];
            var horizons = [];

            // get horizon
            getHorizon(pt, null, face, horizons);

            // Add new faces
            var firstSide = null, prevSide;
            var newFaces = [];
            for (var i = 0; i < horizons.length; i++) {
                var horizon = horizons[i];
                var newFace = new Face([new HalfEdge(pt, ptIdx), new HalfEdge(horizon.vertex, horizon.idx), new HalfEdge(horizon.next.vertex, horizon.next.idx)]);
                newFace.edge[1].setOpposite(horizon.opposite);
                var sideEdge = newFace.edge[2];
                if (!firstSide) {
                    firstSide = sideEdge;
                } else {
                    sideEdge.next.setOpposite(prevSide);
                }
                newFaces.push(newFace);
                prevSide = sideEdge;
            }
            firstSide.next.setOpposite(prevSide);

            // Reassign points
            for (var i = 1; i < face.visibles.length; i++) {
                for (var j = 0; j < newFaces.length; j++) {
                    if (newFaces[j].visible(points[face.visibles[i]])) {
                        newFaces[j].visibles.push(face.visibles[i]);
                        break;
                    }
                }
            }

            // Add faces to hull/stack
            for (var j = 0; j < newFaces.length; j++) {
                Faces.push(newFaces[j]);
                if (newFaces[j].visibles.length > 0) {
                    FaceStk.push(Faces.length - 1);
                }
            }
        }

        // Filtered old face
        var NewFaces = [];
        for (var i = 0; i < Faces.length; i++) {
            if (Faces[i].active)
                NewFaces.push(Faces[i]);
        }

        return NewFaces;
    }

    function getHorizon(eye, cross, face, horizon) {
        face.active = false;
        var edge = !cross ? (cross = face.edge[0]) : cross.next;

        do {
            if (edge.opposite.face.active) {
                if (edge.opposite.face.visible(eye)) {
                    getHorizon(eye, edge.opposite, edge.opposite.face, horizon);
                } else {
                    horizon.push(edge);
                }
            }
            edge = edge.next;
        } while (edge !== cross);
    }

    function Delaunay(points) {
        var Faces = Hull(points);

        for (var i = 0; i < Faces.length; i++) {
            Faces[i].center = Faces[i].circumcenter();
        }

        return Faces;
    }

    function Voronoi(points) {
        var Faces = Delaunay(points);

        // Mark all edge
        for (var i = 0; i < Faces.length; i++) {
            for (var j = 0; j < Faces[i].edge.length; j++) {
                Faces[i].edge[j].marked = true;
                Faces[i].edge[j].crossEdge = null;
            }
            Faces[i].idx = i;
        }

        var NewFaces = [];
        for (var i = 0; i < Faces.length; i++) {
            var face = Faces[i];
            for (var j = 0; j < face.edge.length; j++) {
                if (face.edge[j].marked) {
                    var heList = [];
                    var edge = face.edge[j];
                    do {
                        if (edge.marked)
                            edge.marked = false;
                        else
                            throw Error("WTF?");
                        var he = new HalfEdge(edge.face.center, edge.face.idx);
                        if (edge.crossEdge) {
                            he.setOpposite(edge.crossEdge);
                        } else {
                            edge.opposite.crossEdge = he;
                        }
                        heList.push(he);
                        edge = edge.opposite.next;
                    } while (edge.face != face);

                    var F = new Face(heList);
                    F.ref_point = face.edge[j].vertex;
                    F.ref_point_idx = face.edge[j].idx;
                    NewFaces.push(F);
                }
            }
        }

        return [Faces, NewFaces];
    }

    function RandomPointSphere(n, random) {
        if (!random)
            random = new MersenneTwister();

        var PointList = [];
        for (var i = 0; i < n; i++) {
            var lat = (2 * random.random() - 1) * Math.PI;
            var z = 2 * random.random() - 1;
            var u = Math.sqrt(1 - z * z);

            var x = Math.cos(lat) * u;
            var y = Math.sin(lat) * u;

            PointList.push(new Geo3D.Vec3(x, y, z));
        }

        return PointList;
    }

    Geo3D.Vec3 = Vector;
    Geo3D.Face3 = Face;
    Geo3D.HalfEdge = HalfEdge;
    Geo3D.ConvexHull = Hull;
    Geo3D.Delaunay = Delaunay;
    Geo3D.Voronoi = Voronoi;

    Geo3D.RandomPointSphere = RandomPointSphere;
})(Geo3D);
