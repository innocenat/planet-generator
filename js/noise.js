
Noise = {};

(function (Noise, Geo3D) {
    // Reference: http://mrl.nyu.edu/~perlin/noise/
    //            http://riven8192.blogspot.com/2010/08/calculate-perlinnoise-twice-as-fast.html

    function PerlinNoise(mt) {
        if (!mt)
            mt = new MersenneTwister();

        this.Permutation = [151,160,137,91,90,15,                                           // Hash lookup table as defined by Ken Perlin.  This is a randomly
            131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,    // arranged array of all numbers from 0-255 inclusive.
            190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
            88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
            77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
            102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
            135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
            5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
            223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
            129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
            251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
            49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
            138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180];

        // Shuffle
        for (var i = 0; i < 256; i++) {
            var idx = mt.rand_int(i+1, 256);
            var t = this.Permutation[idx];
            this.Permutation[idx] = this.Permutation[i];
            this.Permutation[i] = t;
        }

        for (var i = 0; i < 256; i++) {
            this.Permutation.push(this.Permutation[i]);
        }
    }

    function grad(hash, x, y, z) {
        switch (hash & 15) {
            case 0: return x+y;
            case 1: return -x+y;
            case 2: return x-y;
            case 3: return -x-y;

            case 4: return x+z;
            case 5: return -x+z;
            case 6: return x-z;
            case 7: return -x-z;

            case 8: return y+z;
            case 9: return -y+z;
            case 10: return y-z;
            case 11: return -y-z;

            case 12: return y+x;
            case 13: return -y-z;
            case 14: return y-x;
            case 15: return -y-z;
        }
    };

    function smooth(t) {
        // perlin interpolation
        // z = 6t^5-15t^4+10t^3
        return t * t * t * (10 + t * (-15 + t * 6));
    }

    function lerp(a, b, x) {
        return a + x*(b-a);
    }

    PerlinNoise.prototype.noise = function(x, y, z) {

        var xi = Math.floor(x) & 255;
        var yi = Math.floor(y) & 255;
        var zi = Math.floor(z) & 255;

        var xf = x-xi;
        var yf = y-yi;
        var zf = z-zi;

        var u = smooth(xf);
        var v = smooth(yf);
        var w = smooth(zf);

        //console.log(xi,yi,zi);
        
        var aaa = this.Permutation[this.Permutation[this.Permutation[    xi ]+    yi ]+    zi ];
        var aba = this.Permutation[this.Permutation[this.Permutation[    xi ]+ (yi+1)]+    zi ];
        var aab = this.Permutation[this.Permutation[this.Permutation[    xi ]+    yi ]+ (zi+1)];
        var abb = this.Permutation[this.Permutation[this.Permutation[    xi ]+ (yi+1)]+ (zi+1)];
        var baa = this.Permutation[this.Permutation[this.Permutation[ (xi+1)]+    yi ]+    zi ];
        var bba = this.Permutation[this.Permutation[this.Permutation[ (xi+1)]+ (yi+1)]+    zi ];
        var bab = this.Permutation[this.Permutation[this.Permutation[ (xi+1)]+    yi ]+ (zi+1)];
        var bbb = this.Permutation[this.Permutation[this.Permutation[ (xi+1)]+ (yi+1)]+ (zi+1)];

        var u1 = lerp(grad(aaa, xf, yf  , zf  ), grad(baa, xf-1, yf  , zf  ), u);
        var u2 = lerp(grad(aba, xf, yf-1, zf  ), grad(bba, xf-1, yf-1, zf  ), u);
        var u3 = lerp(grad(aab, xf, yf  , zf-1), grad(bab, xf-1, yf  , zf-1), u);
        var u4 = lerp(grad(abb, xf, yf-1, zf-1), grad(bbb, xf-1, yf-1, zf-1), u);
        var v1 = lerp(u1, u2, v);
        var v2 = lerp(u3, u4, v);
        var w1 = lerp(v1, v2, w);
        return w1;

    };

    function PerlinOctave(n, persist, mt) {
        this.octaveInfo = [];

        if (!mt)
            mt = new MersenneTwister();

        var p = 1,f=1;
        for (var i = 0; i < n; i++) {
            var octave = {
                noise: new PerlinNoise(mt),
                freq: f,
                amplitude: p
            };
            p /= persist;
            f *= 2;

            this.octaveInfo.push(octave);
        }
    }

    PerlinOctave.prototype.noise = function (x,y,z) {
        var r = 0;
        for (var i = 0; i < this.octaveInfo.length; i++) {
            var octave = this.octaveInfo[i];
            var f = octave.freq;
            r += octave.amplitude*octave.noise.noise(x*f, y*f, z*f);
        }
        return r;
    };

    Noise.Perlin = PerlinNoise;
    Noise.Octave = PerlinOctave;
})(Noise, Geo3D);
